import './App.css';
import {
  Switch,
  Route,
} from "react-router-dom";
import { members } from './members';

import Customer from './pages/customer';
import Company from './pages/company';

import Links from './components/Links'

function App() {

  return (
    <div className="App">
      <header className="App-header">
      <Switch>
          <Route path="/customer/:id">
            <Customer members={ members } />
          </Route>
          <Route path="/company/:id">
            <Company members={ members } />
          </Route>
          <Route path="/">
            <Links members={ members } />
          </Route>
        </Switch>
      </header>
    </div>
  );
}

export default App;
