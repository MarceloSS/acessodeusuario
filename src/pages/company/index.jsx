import { useParams, Link } from "react-router-dom";

const Company = (props) => {
    const { id } = useParams();

    const member = props.members.find(member => member.id.toString() === id)
    
    return(
        <div>
            <h1>Detalhes da Empresa</h1>
            <p>Nome da empresa: {member && member.name}</p>
            <Link to="/">Voltar</Link>
        </div>  
    )   
}

export default Company;