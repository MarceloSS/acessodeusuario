import { useParams, Link } from "react-router-dom";

const Customer = (props) => {
    const { id } = useParams();

    const member = props.members.find(member => member.id.toString() === id)

    return(
        <div>
            <h1>Detalhes do cliente</h1>
            <p>Nome: {member && member.name}</p>
            <Link to="/">Voltar</Link>
        </div>   
    )   
}

export default Customer;