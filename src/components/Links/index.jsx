import { Link } from 'react-router-dom';

const Links = (props) => {
    return(
        <ul>
            {props.members.map( member => {
                return(

                    <li>
                        {member.type === "pj" ? 
                        <Link to={`company/${member.id}`}>{member.name}</Link>
                        :
                        <Link to={`customer/${member.id}`}>{member.name}</Link>
                    }
                    </li>
                )
            })}
        </ul>
        )
}

export default Links;